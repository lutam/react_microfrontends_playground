import axios from 'axios';

const formatUrl = endpoint => {
  const apiUrl = 'https://api.thecatapi.com/v1/';
  return apiUrl + endpoint;
};

const headers = { 'x-api-key': '2e74d898-e4fa-4219-b375-06291c091370' };

export const get = endpoint => axios.get(formatUrl(endpoint), { headers });
