import React from 'react';
import UseTranslations from '../../../hooks/translations.hook';

const SubmitButton = (props) => {
  const { isLoading } = props;
  const { t } = UseTranslations();

  return (
    <button
      onClick={() => props.onClick()}
      className={`w-100 btn btn-primary ${isLoading && 'disabled'}`}
      type="button"
    >
      {isLoading
        ? (<span>Loading</span>)
        : (<span>{t('cta.load_more')}</span>)}
    </button>
  );
};

export default SubmitButton;
