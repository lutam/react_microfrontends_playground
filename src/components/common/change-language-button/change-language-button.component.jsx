import React from 'react';
import UseTranslations from '../../../hooks/translations.hook';

const ChangeLanguageButton = props => {
  const { t, getOtherLanguage } = UseTranslations();

  return (
    <button
      onClick={() => props.onClick()}
      className="w-100 small btn btn-outline-primary"
      type="button"
    >
      {t('cta.switch_to')}
      <span className="ml-1">{getOtherLanguage()}</span>
    </button>
  );
};

export default ChangeLanguageButton;
