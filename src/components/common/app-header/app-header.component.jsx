import React from 'react';
import { Link } from 'react-router-dom';
import logo from 'assets/images/logo.svg';
import './app-header.component.scss';
import ChangeLanguageButton from '../change-language-button/change-language-button.component';

const AppHeader = props => (
  <div id="app-header">
    <div className="container">
      <div className="row">
        <div className="col-md-8 offset-md-2">
          <div className="row">
            <div className="col-md-6 offset-md-3">
              <Link to="/">
                <h1>
                  <img className="app-logo" src={logo} title="Playground app" alt="app logo" />
                </h1>
              </Link>
            </div>
            <div className="col-md-3">
              <div className="buttons">
                <ChangeLanguageButton
                  onClick={() => props.onChangeLanguage()}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default AppHeader;
