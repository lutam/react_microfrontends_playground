import React from 'react';
import './loading-placeholder.component.scss';
import UseTranslations from '../../../hooks/translations.hook';

const LoadingPlaceholder = () => {
  const { t } = UseTranslations();

  return (
    <div className="spinner-container">
      <div className="spinner-border text-dark" role="status" />
      <span>
        {t('loading')}...
      </span>
    </div>
  );
};

export default LoadingPlaceholder;
