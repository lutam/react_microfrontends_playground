import React from 'react';
import {
  Switch, Route, BrowserRouter,
} from 'react-router-dom';
import CatsList from './cats/cats-list/cats-list.component';
import CatDetails from './cats/cat-details/cat-details.component';
import AppHeader from './common/app-header/app-header.component';
import UseTranslations from '../hooks/translations.hook';


const App = () => {
  const { switchLanguage } = UseTranslations();

  return (
    <div id="app" className="mb-4">
      <BrowserRouter>
        <AppHeader onChangeLanguage={switchLanguage} />
        <Switch>
          <Route path="/cat/:id" component={CatDetails} />
          <Route exact path="/" component={CatsList} />
        </Switch>
      </BrowserRouter>
    </div>
  );
};

export default App;
