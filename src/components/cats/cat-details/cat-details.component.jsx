import React from 'react';
import { Link } from 'react-router-dom';
import UseCatDetails from '../../../hooks/cat-details.hook';
import LoadingPlaceholder from '../../common/loading-placeholder/loading-placeholder.component';
import './cat-details.component.scss';
import UseTranslations from '../../../hooks/translations.hook';


const CatDetails = (props) => {
  const { match } = props;
  const { catDetails, isLoadingPage } = UseCatDetails(match.params.id);
  const { t } = UseTranslations();


  return (
    <div className="container">
      {isLoadingPage
        ? (
          <LoadingPlaceholder />
        )
        : (
          <section className="cat-details">
            <div className="row">
              <div className="col-md-6 offset-3 mt-3">
                <nav aria-label="breadcrumb">
                  <ol className="breadcrumb">
                    <li className="breadcrumb-item">
                      <Link to="/">
                        {t('breeds_list')}
                      </Link>
                    </li>
                    <li className="breadcrumb-item active" aria-current="page">{catDetails.name}</li>
                  </ol>
                </nav>
              </div>
            </div>

            <div className="row">
              <div className="col-md-6 offset-3 mt-3">
                <div className="card p-3">
                  <header className="cat-details--header">
                    <h3>{catDetails.name}</h3>
                    <img alt={catDetails.name} src={catDetails.images[0].url} />
                  </header>

                  <div className="cat-details--main">
                    <div>
                      <p className="label">
                        {t('origin')}
                        :
                      </p>
                      <p>{catDetails.origin}</p>
                    </div>
                    <div>
                      <p className="label">
                        {t('temperament')}
                        :
                      </p>
                      <p>{catDetails.temperament}</p>
                    </div>
                    <div>
                      <p className="label">
                        {t('description')}
                        :
                      </p>
                      <p>{catDetails.description}</p>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </section>
        )}
    </div>
  );
};

export default CatDetails;
