import React from 'react';
import { Link } from 'react-router-dom';
import './cat-card.component.scss';

const CatCard = (props) => {
  const { cat } = props;

  return (
    <div className="col-md-6">
      <Link to={`cat/${cat.id}`}>
        <div className="cat-card card p-2 m-2">
          {cat.name}
        </div>
      </Link>
    </div>
  );
};

export default CatCard;
