import React from 'react';
import CatCard from '../cat-card/cat-card.component';
import SubmitButton from '../../common/submit-button/submit-button.component';
import UseCatsList from '../../../hooks/cats-list.hook';
import LoadingPlaceholder from '../../common/loading-placeholder/loading-placeholder.component';
import UseTranslations from '../../../hooks/translations.hook';

const CatsList = () => {
  const {
    isLoadingPage, cats, loadMore, isLoadingCats,
  } = UseCatsList();

  const { t } = UseTranslations();

  return (
    <div>
      <div className="container">
        {isLoadingPage
          ? (
            <LoadingPlaceholder />
          )
          : (
            <>
              <div className="row">
                <div className="col-md-6 offset-md-3">
                  <div className="row">
                    <h2 className="col-12 mt-3 mb-3">
                      {t('breeds_list')}
                    </h2>
                  </div>
                  <div className="row">
                    {cats.map(cat => <CatCard key={cat.id} cat={cat} />)}
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="mt-3 col-4 offset-4">
                  <SubmitButton
                    isLoading={isLoadingCats}
                    onClick={loadMore}
                  />
                </div>
              </div>
            </>
          )}
      </div>
    </div>

  );
};

export default CatsList;
