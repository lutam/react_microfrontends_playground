import React from 'react';
import { shallow } from 'enzyme';
import App from './cats-list.component';

it('should display two link items', () => {
  const wrapper = shallow(<App />);
  expect(wrapper.find('Link').length).toBe(2);
});
