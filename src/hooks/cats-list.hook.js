import React from 'react';
import { get } from '../services/api.service';

const UseCatsList = () => {
  const catsRequestLimit = 8;
  const [catsRequestPage, setCatsRequestPage] = React.useState(0);
  const [cats, setCats] = React.useState([]);
  const [isLoadingCats, setIsLoadingCats] = React.useState(true);
  const [isLoadingPage, setIsLoadingPage] = React.useState(true);

  React.useEffect(() => {
    const fetchCats = () => {
      const url = `breeds?limit=${catsRequestLimit}&page=${catsRequestPage}`;
      setIsLoadingCats(true);
      get(url)
        .then(fetchedCats => {
          setCats(currentCats => [...currentCats, ...fetchedCats.data]);
          setIsLoadingCats(false);
          setIsLoadingPage(false);
        });
    };
    fetchCats();
  }, [catsRequestPage]);

  const loadMore = () => {
    setCatsRequestPage(currentPage => currentPage + 1);
  };

  return {
    isLoadingCats, catsRequestPage, isLoadingPage, cats, loadMore,
  };
};


export default UseCatsList;
