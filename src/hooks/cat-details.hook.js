import React from 'react';
import { get } from '../services/api.service';

const UseCatDetails = (breedId) => {
  const [catDetails, setCatDetails] = React.useState({});
  const [isLoadingPage, setIsLoadingPage] = React.useState(true);


  React.useEffect(() => {
    const fetchCatDetails = () => {
      const endpoint = `breeds/${breedId}`;
      get(endpoint)
        .then(fetchedCat => {
          const imagesEndpoint = `images/search?breed_id=${breedId}&size=med`;
          get(imagesEndpoint).then(fetchedCatImage => {
            setCatDetails({ ...fetchedCat.data, images: fetchedCatImage.data });
            setIsLoadingPage(false);
          });
        });
    };

    fetchCatDetails();
  }, [breedId]);


  return {
    catDetails, isLoadingPage,
  };
};


export default UseCatDetails;
