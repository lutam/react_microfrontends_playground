import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/app.component';

import './assets/styles/main.scss';


ReactDOM.render(<App />, document.getElementById('root'));
